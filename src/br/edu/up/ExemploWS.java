package br.edu.up;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/ws")
public class ExemploWS {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getTexto(){
		return "Ol� JAX-RS!";
	}
}